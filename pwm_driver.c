#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/poll.h>
#include <asm/irq.h>
#include <asm/io.h>
#include <linux/interrupt.h>
#include <asm/uaccess.h>
#include <linux/clk.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/miscdevice.h>

#define DEVICE_NAME "nano_pwm"



#define PWM_CON 0x7000a000
#define PWM_CLK_SRC 0x60006110
#define PWM_CLK_SRC_EN 0x60006010
#define PWM_MPIO_TRISTATE 0x700031fc 

static struct semaphore lock;

static int nano_pwm_open(struct inode *inode, struct file *file)
{
	if (!down_trylock(&lock))
		return 0;
	else
		return -EBUSY;
}

static int nano_pwm_close(struct inode *inode, struct file *file)
{
	up(&lock);
	return 0;
}

void PWM_Stop( void )
{
	void __iomem* pwm_con = ioremap(PWM_CON, 4);
	uint32_t data;
	data = ioread32(pwm_con);
	data &= (~(1 << 31));
	iowrite32(data, pwm_con);
	printk(KERN_INFO "STOPPING PWM\n");
}

static bool pwm_freq_is_set = false;

static void PWM_set_freq(uint32_t freq)
{
	void __iomem* pwm_con;
	void __iomem* pwm_tristate;
	// CLK PLL = 408MHZ, Divide by N/2 + 1, default N = 15, so divide by 8.5
	// Fref_clk = 48,000,000
	uint32_t test_data;
	uint32_t Fref_clk;
	int32_t pfm;
	uint32_t bit_pattern;
 	uint32_t pwm_mpio_data;
	// CALCULATE PFM to get PWM
	// PFM = max(min(round((Fref_clk / 256)/Fpwm_out)-1, 0x1fff), 0x0)
	// ADD 0.5f to round up
	Fref_clk = ((48000000 / 256) / (freq)) ;

	// Truncate float
	pfm = (Fref_clk) - 1;
	pfm = pfm <= 0x1fff ? pfm : 0x1fff;
	pfm = pfm >= 0x0 ? pfm : 0x0;
	printk(KERN_INFO "PFM = %d\n", pfm);

	pwm_con = ioremap(PWM_CON,4);
	test_data = ioread32(pwm_con);
	test_data &= (0xFFFFE000);
	bit_pattern = 0;
	bit_pattern |= ((uint16_t)pfm & 0x1fff);
	test_data |= bit_pattern;
  
	iowrite32(test_data, pwm_con);
	test_data = ioread32(pwm_con);
	printk(KERN_INFO "PWM CONF set to %x\n", test_data);
	iounmap(pwm_con);
	

	if(!pwm_freq_is_set){
		pwm_tristate = ioremap(PWM_MPIO_TRISTATE, 4);
		pwm_mpio_data = ioread32(pwm_tristate);
		pwm_mpio_data &= ( ~ (1 << 4));
		iowrite32(pwm_mpio_data, pwm_tristate);
		printk(KERN_INFO "PWM MUX OUTPUT:: %x\n", ioread32(pwm_tristate));
		iounmap(pwm_tristate);
		pwm_freq_is_set = true;
	} 
	

}
static void PWM_set_duty_cycle(uint32_t duty_cycle)
{

	uint32_t test_data, bit_pattern;
	void __iomem* pwm_con;
	int32_t percentage, pwm;
	pwm_con = ioremap(PWM_CON,4);


	// Calculate Duty Cycle
	// PWM = max(min(round(percent/100 * 256), 256), 0)
	percentage = (duty_cycle * 256) / 100;
	pwm = percentage;
	pwm = pwm < 256 ? pwm : 256;
	pwm = pwm > 0 ? pwm : 0;

	printk(KERN_INFO "PWM = %d\n", pwm);
	test_data = ioread32(pwm_con);
	test_data &= (0xFE00FFFF);
	bit_pattern = 0;
	if(pwm == 256){
		bit_pattern |= (1 << 24);
	} else {
		bit_pattern |= ( ((uint8_t)(pwm)) << 16);
	}
	test_data |= bit_pattern;
	// Enable pwm
	test_data |= (1 << 31);
  
	iowrite32(test_data, pwm_con);
	test_data = ioread32(pwm_con);
	printk(KERN_INFO "PWM CONF after duty cycle is set is %x\n", test_data);
	iounmap(pwm_con);

}
static long nano_pwm_ioctl(struct file *filep, unsigned int cmd, unsigned long arg){
	enum cmds {pwm_set_freq = 256, pwm_set_duty_cycle, pwm_stop};
	printk(KERN_INFO "REACHED NANO_PWM_IOCTL:CMD = %d\n",cmd);
	switch(cmd){
		case pwm_set_freq:
			if(arg == 0)
				return -EINVAL;
			printk(KERN_INFO "CALLING pwm_set_freq\n");
			PWM_set_freq(arg);
			break;
		
		case pwm_set_duty_cycle:
			if(arg == 0)
				return -EINVAL;
			printk(KERN_INFO "CALLING pwm_set_duty_cycle\n");
			PWM_set_duty_cycle(arg);
			break;
		default:
			PWM_Stop();
			break;
	}
	return 0;
}



struct file_operations fops = {
	.owner = THIS_MODULE,
	.open = nano_pwm_open,
	.release = nano_pwm_close,
	.unlocked_ioctl = nano_pwm_ioctl,
};

static struct miscdevice misc = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = DEVICE_NAME,
	.fops = &fops,
};


static int init_pwm(void)
{
	int ret;

	sema_init(&lock, 1);
	ret = misc_register(&misc);
	printk (DEVICE_NAME"\tinitialized with minor number:%d\n", misc.minor);
    	return ret;

}

static void exit_pwm(void)
{
	misc_deregister(&misc);
}

module_init(init_pwm);
module_exit(exit_pwm);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jonathan Tran");
MODULE_DESCRIPTION("Simple GPIO Output");
