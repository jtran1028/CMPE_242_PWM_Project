# CMPE 242 PWM Project

A driver for PWM on the Jetson Nano along with a User space program to make system calls to the device driver to change frequency and duty cycle.